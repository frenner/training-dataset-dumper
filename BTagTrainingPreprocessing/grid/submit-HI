#!/usr/bin/env bash

# This script should not be sourced, we don't need anything in here to
# propigate to the surrounding environment.
if [[ $- == *i* ]] ; then
    echo "Don't source me!" >&2
    return 1
else
  # set the shell to exit if there's an error (-e), and to error if
  # there's an unset variable (-u)
    set -eu
fi


##########################
# Real things start here #
##########################
#
# part 0 is parsing arguments
#
# Some default values
#
P=10
#
MODE=default
#
# Add some mode switches
declare -A DEFAULT_CONFIGS_BY_MODE=(
    [default]=upgrade-HI.json
)
declare -A SCRIPTS_BY_MODE=(
    [default]=ca-dump-upgrade-HI
)
#
INPUT_DATASETS=(
#    mc16_5TeV.800893.Py8EG_A14N23LO_jetjet_JZ1WithSW_bfilter.merge.AOD.e8366_d1521_r11472_r11217
#    mc16_5TeV.800894.Py8EG_A14N23LO_jetjet_JZ2WithSW_bfilter.merge.AOD.e8366_d1521_r11472_r11217
#    mc16_5TeV.800895.Py8EG_A14N23LO_jetjet_JZ3WithSW_bfilter.merge.AOD.e8366_d1521_r11472_r11217
#    mc16_5TeV.800896.Py8EG_A14N23LO_jetjet_JZ4WithSW_bfilter.merge.AOD.e8366_d1521_r11472_r11217
    mc16_5TeV.800897.Py8EG_A14N23LO_jetjet_JZ5WithSW_bfilter.merge.AOD.e8366_d1521_r11472_r11217
)

function _help() {
    local DEF_CFG_DEF_MODE=${DEFAULT_CONFIGS_BY_MODE[$MODE]}
    cat <<EOF
usage: ${0##*/} [-h] [options] [CONFIG_FILE=${DEF_CFG_DEF_MODE}}]

Submit a job to the grid! The CONFIG_FILE is json, and should live
under /configs in this package. Note that this script will ask you to
commit your changes, but it's a good idea to tag them as well.

Options:
 -f: Force, don't require changes to be committed.
 -p <n>: number of parallel submissions to run (default $P)
 -d: Dry run, don't submit anything or make a tarball, but build the
     submit directory.
 -i <file>: File listing input files, will override the default list.
 -t: run a test job, only one file per dataset
 -a <tag>: annotate output with an additional tag
 -m <mode>: set run mode (options: ${!SCRIPTS_BY_MODE[@]})

Run mode options:
EOF
    local M
    local F1="       "           # <-- padding for the first field
    local F2="                 " # <-- padding for the second field
    for M in ${!DEFAULT_CONFIGS_BY_MODE[@]} ; do
        local CFG=${DEFAULT_CONFIGS_BY_MODE[$M]}
        local SCR=${SCRIPTS_BY_MODE[$M]}
        echo " $M${F1:${#M}} ==>  config: $CFG${F2:${#CFG}}  script: $SCR"
    done
    cat <<EOF

With the current arguments, will run over:
EOF
    local F
    for F in ${INPUT_DATASETS[@]} ; do
        echo $F
    done
}

FORCE=''
# this might be 'echo' to do a dry run
PRE=''
EXTRA_ARGS=''
# any additional dataset names. Note that anyone who adds to this has
# to provide the leading `.`
ANNOTATE=''
while getopts ":hfp:di:ta:m:" opt $@;
do
    case $opt in
        h) _help; exit 1;;
        f) FORCE=1 ;;
        p) P=${OPTARG} ;;
        d) PRE="echo DRY-RUNNING: " ;;
        i) readarray -t INPUT_DATASETS < ${OPTARG} ;;
        t) EXTRA_ARGS+=' --nFiles 1 '; ANNOTATE+=.test ;;
        a) ANNOTATE+=.${OPTARG} ;;
        m) MODE=${OPTARG} ;;
    esac
done
shift $((OPTIND-1))


###################################################
# Part 1: variables you you _might_ need to change
###################################################
#
# Users's grid name
GRID_NAME=${RUCIO_ACCOUNT-${USER}}
#


######################################################
# Part 2: variables you probably don't have to change
######################################################
#
# Build a zip of the files we're going to submit
ZIP=job.tgz
#
# This is the subdirectory we submit from
SUBMIT_DIR=submit
#
# This is where all the source files are
BASE=$(dirname $(readlink -e ${BASH_SOURCE[0]}))/../..
#
# Configuration file stuff
DEFAULT_CONFIG=${DEFAULT_CONFIGS_BY_MODE[$MODE]}
DEFAULT_CONFIG_PATH=${BASE}/configs/single-b-tag/${DEFAULT_CONFIG}
#
# The executable
EXE=${SCRIPTS_BY_MODE[$MODE]}
#
# Check that we don't have uncontrolled changes
export BASE
GIT_TAG=$(
    cd $BASE
    if ! git diff-index --quiet HEAD; then
        if [[ $FORCE ]]; then
            date +%F-T%H%M%S
            exit 0
        fi
        echo "ERROR: uncommitted changes, please commit them" >&2
        exit 1
    fi
    git describe
)

###################################################
# Part 3: prep the submit area
###################################################
#
echo "preping submit area"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi
mkdir ${SUBMIT_DIR}
CONFIG_PATH=${1-${DEFAULT_CONFIG_PATH}}
echo "using config file ${CONFIG_PATH}"
cp ${CONFIG_PATH} ${SUBMIT_DIR}
# make sure we send files that the configuration depends on too
cp -r ${DEFAULT_CONFIG_PATH%/*}/fragments ${SUBMIT_DIR}
cd ${SUBMIT_DIR}


##########################################
# Part 4: build a tarball of the job
###########################################
#
# Check to make sure you've properly set up the environemnt: if you
# haven't sourced the setup script in the build directory the grid
# submission will fail, so we check here before doing any work.
if ! type $EXE &> /dev/null ; then
    echo "You haven't sourced x86*/setup.sh, job will fail!" >&2
    echo "quitting..." >&2
    exit 1
fi
#
echo "making tarball of local files: ${ZIP}" >&2
#
# The --outTarBall, --noSubmit, and --useAthenaPackages arguments are
# important. The --outDS and --exec don't matter at all here, they are
# just placeholders to keep panda from complianing.
${PRE} prun --outTarBall=${ZIP} --noSubmit --useAthenaPackages\
     --exec "ls"\
     --outDS user.${GRID_NAME}.x

##########################################
# Part 5: loop over datasets and submit
##########################################

# Loop over all inputs
echo "submitting for ${#INPUT_DATASETS[*]} datasets"
#
# define a fucntion to do all the submitting
function submit-job() (
    set -eu
    DS=$1
    # This regex extracts the DSID from the input dataset name, so
    # that we can give the output dataset a unique name. It's not
    # pretty: ideally we'd just suffix our input dataset name with
    # another tag. But thanks to insanely long job options names we
    # use in the generation stage we're running out of space for
    # everything else.
    DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
    #
    # Build the full output dataset name
    CONFIG_FILE=${CONFIG_PATH##*/}
    #
    # this terrible regex extracts the atlas tags, e.g. e4342_s3443...
    ATLAS_TAGS=$(egrep -o '(_?[esdfarp][0-9]{3,6}){3,}' <<< ${DS})
    TAGS=${ATLAS_TAGS}.${CONFIG_FILE%.*}.${AtlasBuildStamp}
    OUT_DS=user.${GRID_NAME}.${DSID}.${TAGS}.${GIT_TAG}${ANNOTATE}
    #
    # Check to make sure the grid name isn't too long
    if [[ $(wc -c <<< ${OUT_DS}) -ge 120 ]] ; then
        echo "ERROR: dataset name ${OUT_DS} is too long, can't submit!" 1>&2
        return 1
    fi
    #
    # Now submit.
    #
    echo "Submitting for ${GRID_NAME} on ${DS} -> ${OUT_DS}"
    ${PRE} prun --exec "${EXE} %IN -c ${CONFIG_FILE}"\
         --outDS ${OUT_DS} --inDS ${DS}\
         --useAthenaPackages --inTarBall=${ZIP}\
         --mergeScript="hdf5-merge-nolock -o %OUT -i %IN"\
         --outputs output.h5\
         --nFiles 2\
         --noEmail \
         ${EXTRA_ARGS} > ${OUT_DS}.log 2>&1
)
#
# we have to export some environment variables so xargs can read them
# below
#
export -f submit-job
export CONFIG_PATH GRID_NAME GIT_TAG ZIP AtlasBuildStamp EXE PRE EXTRA_ARGS
export ANNOTATE
#
# Use xargs to submit all these jobs in batches of $P
printf "%s\n" ${INPUT_DATASETS[*]} | xargs -P $P -I {} bash -c "submit-job {}"
