/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGGER_JET_MATCHER_ALG_H
#define TRIGGER_JET_MATCHER_ALG_H

#include "xAODBTagging/BTaggingContainer.h"
#include "xAODJet/JetContainer.h"

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/WriteDecorHandleKeyArray.h"
#include "StoreGate/ReadDecorHandleKeyArray.h"

#include <type_traits>

template <typename T>
struct MatchedPair
{
  typename T::const_value_type from;
  typename T::const_value_type to;
};

template <typename T, typename C = xAOD::BTaggingContainer>
class VariableMule
{
public:
  std::map<std::string,std::string> toCopy;
private:
  using RC = SG::ReadDecorHandleKeyArray<C>;
  using WC = SG::WriteDecorHandleKeyArray<C>;
  RC m_fromKeys;
  WC m_toKeys;
public:
  StatusCode initialize(Gaudi::Algorithm* parent,
                        const std::string& from,
                        const std::string& to) {
    for (const auto& key: toCopy) {
      std::string fullfrom = from + "." + key.first;
      std::string fullto = to + "." + key.second;
      std::string doc = "Key to move " + key.first + "->" + key.second;
      m_fromKeys.emplace_back(parent, "read_" + key.first, fullfrom, doc);
      m_toKeys.emplace_back(parent, "write_" + key.second, fullto, doc);
    }
    ATH_CHECK(m_fromKeys.initialize());
    ATH_CHECK(m_toKeys.initialize());
    return StatusCode::SUCCESS;
  }
  void copy(const std::vector<MatchedPair<C>>& pairs, const EventContext& cxt)
    const {
    std::vector<SG::ReadDecorHandle<C,T>> from;
    std::vector<SG::WriteDecorHandle<C,T>> to;
    size_t n_keys = m_fromKeys.size();
    for (size_t iii = 0; iii < n_keys; iii++) {
      const auto& fromKey = m_fromKeys.at(iii);
      const auto& toKey = m_toKeys.at(iii);
      from.emplace_back(fromKey, cxt);
      to.emplace_back(toKey, cxt);
    }
    for (const auto& pair: pairs) {
      for (size_t iii = 0; iii < n_keys; iii++) {
        if (pair.from) {
          to.at(iii)(*pair.to) = from.at(iii)(*pair.from);
        } else {
          const bool is_fp = std::is_floating_point<T>::value;
          static_assert(std::is_integral<T>::value || is_fp,
                        "we only supprt floating and integral types");
          if (is_fp) {
            to.at(iii)(*pair.to) = NAN;
          } else {
            to.at(iii)(*pair.to) = -1;
          }
        }
      }
    }
  }
};


class TriggerBTagMatcherAlg: public AthReentrantAlgorithm
{
public:
  TriggerBTagMatcherAlg(const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute (const EventContext&) const override;
  virtual StatusCode finalize () override;
private:
  using JL = ElementLink<xAOD::JetContainer>;
  using BC = xAOD::BTaggingContainer;
  using JC = xAOD::JetContainer;
  std::string m_offlineBtagKey;
  std::string m_triggerBtagKey;
  float m_drMatchMax;
  VariableMule<float> m_floats;
  VariableMule<int> m_ints;
  VariableMule<int, JC> m_jetInts;
  SG::ReadHandleKey<JC> m_triggerJetKey;
  SG::ReadHandleKey<JC> m_offlineJetKey;
  SG::ReadDecorHandleKey<BC> m_triggerBtag;
  SG::ReadDecorHandleKey<BC> m_offlineBtag;
  SG::WriteDecorHandleKey<BC> m_drDecorator;
  SG::WriteDecorHandleKey<BC> m_dPtDecorator;
};

#endif
