#ifndef ADD_METADATA_HH
#define ADD_METADATA_HH

namespace H5 {
  class Group;
}

class OriginalAodCounts;

void addMetadata(H5::Group& grp, const OriginalAodCounts& counts);

#endif
