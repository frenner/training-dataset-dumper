#include "src/ConfigFileTools.hh"

#include <nlohmann/json.hpp>

#include <filesystem>
#include <iostream>
#include <fstream>

int main(int narg, char* argv[]) {
  namespace fs = std::filesystem;
  namespace pt = boost::property_tree;
  if (narg != 2) {
    std::cout << "usage: " << argv[0] << " <json_file>" << std::endl;
    return 1;
  }

  fs::path config_path(argv[1]);
  if (!fs::exists(config_path)) return 2;
  std::ifstream config_stream(config_path.string());
  auto nlotree = nlohmann::ordered_json::parse(config_stream);
  ConfigFileTools::combine_files(nlotree, config_path.parent_path());
  std::cout << nlotree.dump(2) << std::endl;

  return 0;
}
