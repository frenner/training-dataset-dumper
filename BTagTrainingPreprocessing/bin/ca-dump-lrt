#!/usr/bin/env python3

"""
Setup using setup-athena.sh is required for this script.

Demonstration for dumping both the LRT and standard tracks

Since the purposeis to simply dump the track info for DIPS and validation

The following approach is chosen:

- Directly on the jet. No BTagging object is created, all the
  variables are added to the jet directly.

"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg
)

from BTagTrainingPreprocessing import trackUtil as trkUtil

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from GaudiKernel.Configurable import DEBUG, INFO

from BTagTrainingPreprocessing import dumper

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as Formatter
import sys

def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=Formatter)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true')
    parser.add_argument('-i','--event-print-interval', type=int, default=100)
    parser.add_argument('-M','--merge-tracks', action='store_true', help='Merge the standard and large d0 track collections before everything else.')
    return parser.parse_args()

def getSimpleJetTagConfig(flags, nnFile, merge):
    """
    This example tags jets directly: there is no intermediate
    b-tagging object
    """

    ca = ComponentAccumulator()

    # first add the track augmentation to define peragee coordinates
    jet_name = 'AntiKt4EMPFlowJets'
    trackContainer = 'InDetTrackParticles'
    primaryVertexContainer = 'PrimaryVertices'
    simpleTrackIpPrefix = 'simpleIp_'
    ca.addEventAlgo(
        CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            'SimpleTrackAugmenter',
            trackContainer=trackContainer,
            primaryVertexContainer=primaryVertexContainer,
            prefix=simpleTrackIpPrefix,
        )
    )
    if merge:
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                'SimpleTrackAugmenter_ForwardTrack',
                trackContainer="InDetLargeD0TrackParticles",
                primaryVertexContainer=primaryVertexContainer,
                prefix=simpleTrackIpPrefix,
            )
        )
        ca.merge(trkUtil.LRTMerger())
        trackContainer = "InDetWithLRTTrackParticles" 

    # now we assicoate the tracks to the jet
    tracksOnJetDecoratorName = "TracksForMinimalJetTag"
    ca.merge(JetParticleAssociationAlgCfg(
        flags,
        JetCollection=jet_name,
        InputParticleCollection=trackContainer,
        OutputParticleDecoration=tracksOnJetDecoratorName,
    ))

    return ca

def getSingleBTagConfig(flags, config, output, merge):

    ca = ComponentAccumulator()

    nnFile = 'dev/BTagging/20210517/dipsLoose/antikt4empflow/network.json'

    ca.merge(getSimpleJetTagConfig(flags, nnFile, merge))
    ca.merge(dumper.getDumperConfig(config, output))

    return ca


def run():
    args = get_args()

    cfgFlags.Input.Files = args.input_files
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    cfgFlags.Exec.OutputLevel = INFO
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG
        cfgFlags.Exec.DebugStage = 'exec'

    cfgFlags.lock()

    ca = getConfig(cfgFlags)
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(getSingleBTagConfig(cfgFlags, args.config_file, args.output, args.merge_tracks))

    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
