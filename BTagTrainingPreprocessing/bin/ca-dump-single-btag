#!/usr/bin/env python3

"""
Bare bones flavor tagging info dumper

You should consider this the starting point for any more complicated
studies, all it does is set up the basic event loop and schedule the
dataset dumper.
"""

from AthenaConfiguration.MainServicesConfig import (
    MainServicesCfg as getConfig)
from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from GaudiKernel.Configurable import DEBUG, INFO

from BTagTrainingPreprocessing import dumper

from argparse import ArgumentParser
from argparse import RawDescriptionHelpFormatter as Formatter
import sys

def get_args():
    parser = ArgumentParser(description=__doc__, formatter_class=Formatter)
    parser.add_argument('input_files', nargs='+')
    parser.add_argument('-o','--output', default='output.h5')
    parser.add_argument('-c','--config-file', required=True)
    parser.add_argument('-m','--max-events', type=int, nargs='?', const=10)
    parser.add_argument('-d','--debug', action='store_true',
                        help="set output level to DEBUG")
    parser.add_argument('-g','--debugger', action='store_true',
                        help="attach debugger at execute step")
    return parser.parse_args()


def run():
    args = get_args()

    cfgFlags.Input.Files = args.input_files
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG
    if args.debugger:
        dumper.prepGdb()
        cfgFlags.Exec.DebugStage = 'exec'

    cfgFlags.lock()

    ca = getConfig(cfgFlags)

    ca.merge(PoolReadCfg(cfgFlags))

    ca.merge(dumper.getDumperConfig(args.config_file, args.output))

    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
