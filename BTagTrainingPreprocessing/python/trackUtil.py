from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

###############################################################
# Function to merge the standard and lrt tracks
###############################################################
def LRTMerger():
    ca = ComponentAccumulator()
    tool = CompFactory.DerivationFramework.TrackParticleMerger(name = "MergeLRTAndStandard", TrackParticleLocation = ["InDetTrackParticles", "InDetLargeD0TrackParticles"], OutputTrackParticleLocation = "InDetWithLRTTrackParticles", CreateViewColllection  = True)
    
    ca.addPublicTool(tool)
    
    LRTMergeAug = CompFactory.DerivationFramework.CommonAugmentation("InDetLRTMerge", AugmentationTools = tool)

    ca.addEventAlgo(LRTMergeAug)
    return ca

