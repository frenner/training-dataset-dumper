## Configuration file

Jobs are steered via json files that live under [`configs`][cfgs]. Nearly all fields are mandatory and there are no default values.
In cases where the field contains a list, the field can (sometimes) be omitted to specify that the list is empty.

### Fragments

Configuration files contain with one special key: `file`.
If this appears in a json object, the parser will assume the key
gives a path _relative to the current file_. Any entries in the file
will be imported at the same scope where the `file` key appears.
If local keys conflict with imported ones they will be merged, giving
local keys precedence over those imported from the file.

??? info "More on merging"
    Conflicting keys are merged as follows:

       - If the keys point objects (i.e. a `dict` or `map`), the union
         of all keys and values within the objects is taken, and
         conflicting keys are merged recursively.
       - If the keys point lists, the lists are concatenated.
       - If the keys point to anything else (i.e. numbers or strings),
         the local key overwrites the key from the file.

Fragments are a useful way to specify default values, since any values
imported via `file` will be overwritten by local ones.

[cfgs]: {{repo_url}}-/tree/r22/configs

### Other configuration notes

 - The `n_jets_per_event` option will change the output format.
   If this is set to 0 we store one jet per entry. If it's set to
   a value larger than 0 each entry will be `n` jets, and a separate
   object with an `_event` suffix will store event information.

 - Both the top level configuration and the track dumpers require a
   `btagging_link` to be specified. This is the name of the link from
   jets to the `BTagging` object, and has historically been
   `btaggingLink` in most ATLAS code. Exceptionally, in the case of
   tracks, the link can be empty: in this case the tracks will be read
   directly from the jet.

### DL2 Config

The DL2 config is the configuration of taggers you want to apply when dumping the ntuples. Some taggers are already applied on the derivation level, but newer versions or freshly developed models can be added via this configuration. The `dl2_config` is a list of dicts with the different taggers you want to add inside. This can look for example like this:

```json
"dl2_configs": [
    {
        "nn_file_path": "<path>/<to>/<your>/<local>/<network.json>",
        "remapping": {}
    },
    {
        "nn_file_path": "dev/BTagging/20210506r22/umami/antikt4empflow/network.json",
        "remapping": {
            "UMAMI20210506r22_pu": "Umami_r22_pu",
            "UMAMI20210506r22_pc": "Umami_r22_pc",
            "UMAMI20210506r22_pb": "Umami_r22_pb",
            "dips_UMAMI20210506r22_pu": "dips_from_Umami_r22_pu",
            "dips_UMAMI20210506r22_pc": "dips_from_Umami_r22_pc",
            "dips_UMAMI20210506r22_pb": "dips_from_Umami_r22_pb"
        }
    }
```

The first dict here has two entries: The `nn_file_path` and `remapping`. The `nn_file_path` is the path to your model. When you are running locally, you can just add the path to the `network.json`. Keep in mind this needs to be a converted LWTNN model of the network! For the second option, an empty dict is provided. If this is the case, the output variables of the network are based on the LWTNN entries when the model was created.

For the second dict, a specific path is given here. This is a path to a model which is available in the ATLAS FTAG (dev) group area. A list of the available taggers can be found [here](https://ftag.docs.cern.ch/algorithms/available_taggers/). For that, a `PathResolver` is used to find the correct path to the model files. The `PathResolver` looks for the model files in the following order:

* The local directory where you start the job
* `/cvmfs/`
* On https://atlas-groupdata.web.cern.ch/atlas-groupdata/ (this is how it works from the docker images)

When you want to run on the grid, this is one way to use a model that is not in the derivations/xAODs. You just need to provide the path inside the group area, not the full group area absolute path. The path to the group area (to check which models are available) is `/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/`.
If you want to add a tagger to the group area, you can request that your file is added there. An explanation how to make a request can be found [here](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/ASGCalibArea). If you want to add a new tagger from FTAG Algorithms, please contact the FTAG Algorithms conveners via [this](atlas-ftag-algorithms-conveners@cern.ch) email and ask them to do it for you.

The second option, `remapping`, is now a dict with entries. If you want to rename the output of one network to a different name, you can do this here. The dict entry key is the current name of the variable and the entry itself is the new name of the variable in the dumped .h5 files.
